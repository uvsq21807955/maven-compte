package compte;


public class Compte
{

		private double solde,credit,debit;
		
		public Compte(double solde) 
		{
			this.solde=solde;
		}
		
		public double getSolde() 
		{
			return this.solde;
		}
		
		public void ConsultationSolde()
		{
			System.out.println("Votre solde est de : " + this.getSolde()+" £");
		}

		public double getCredit() {
			return credit;
		}

		public void setCredit(double credit) {
			this.credit = credit;
		}

		public double getDebit() {
			return debit;
		}

		public void setDebit(double debit) {
			this.debit = debit;
		}
		
		public void Crediter(double  valeur) 
		{
			this.solde+=valeur;
		}
		
		public void Debiter(double  valeur) 
		{
			this.solde-=valeur;
		}
		
		
		public void Virement(Compte c,double valeur )
		{
			
			if(valeur>0) 
			{
				if(this.solde>valeur) 
				{
					c.Crediter(valeur);
					c.solde+=c.getDebit();
					this.solde-=valeur;
				}
			}
		}
		
		
}
