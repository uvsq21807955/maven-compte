package compte;

import static org.junit.Assert.*;
import org.junit.Test;

public class CompteTest {
	
	@Test
	public void CrediterTest() throws Exception 
	{
		Compte c= new Compte(20);//init
		c.Crediter(10);//appel
		assertEquals(30,c.getSolde(),1e-8);//verification
		
	}
	
	@Test
	public void DebiterTest() throws Exception 
	{
		Compte c= new Compte(20);//init
		c.Debiter(10);//appel
		assertEquals(10,c.getSolde(),1e-8);//verification
		
	}
	
	@Test
	public void virementTest() throws Exception 
	{
		Compte c= new Compte(200);//init
		Compte c17= new Compte(120);
		c.Virement(c17,100);//appel
		assertEquals(100,c.getSolde(),1e-8);//verification
		
	}
}
